import { browser, by, element, ElementFinder } from 'protractor';
import { LessonSelectPageObject } from './lesson-select.po';

export class LessonPageObject {
  lessonSelectPage = new LessonSelectPageObject();

  async navigateTo() {
    await this.lessonSelectPage.navigateTo();
    return this.lessonSelectPage.getLessonListItems().first().click();
  }

  getLessonContent() {
    return element(by.css('.lesson-content'));
  }
}
