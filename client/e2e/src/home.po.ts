import { protractor, browser, by, element, ElementFinder } from 'protractor';

export class HomePageObject {
  async navigateTo() {
    await browser.get('/');
    await browser.wait(protractor.ExpectedConditions.urlContains('home'));

    const listItem = await this.getModuleListItems().first();

    await browser.wait(
      protractor.ExpectedConditions.elementToBeClickable(listItem)
    );
  }

  getModuleListItems() {
    return element.all(by.css('.module-list ion-item'));
  }

  getLogoutButton() {
    return element(by.css('.logout-button'));
  }
}
