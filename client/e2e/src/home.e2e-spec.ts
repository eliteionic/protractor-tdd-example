import { browser, protractor } from 'protractor';
import { HomePageObject } from './home.po';
import { LoginPageObject } from './login.po';
import { LessonSelectPageObject } from './lesson-select.po';

describe('Home', () => {
  let homePage: HomePageObject;
  let loginPage: LoginPageObject;
  let lessonSelectPage: LessonSelectPageObject;

  beforeEach(async () => {
    homePage = new HomePageObject();
    loginPage = new LoginPageObject();
    lessonSelectPage = new LessonSelectPageObject();
    await homePage.navigateTo();
  });

  it('should be able to view a list of modules', async () => {
    const numberOfModulesInList = await homePage.getModuleListItems().count();
    expect(numberOfModulesInList).toBe(5);
  });

  it('the list of modules should contain the titles of the modules', async () => {
    expect(await homePage.getModuleListItems().first().getText()).toContain(
      'Module One'
    );
  });

  it('after selecting a specific module, the user should be able to see a list of available lessons', async () => {
    const moduleToTest = homePage.getModuleListItems().first();

    await moduleToTest.click();

    expect(await lessonSelectPage.getLessonListItems().count()).toBeGreaterThan(
      0
    );
  });

  it('should be able to log out', async () => {
    await homePage.getLogoutButton().click();

    browser.wait(
      protractor.ExpectedConditions.not(
        protractor.ExpectedConditions.urlContains('home')
      )
    );

    /* Log back in to prevent rest of tests breaking */

    const input = loginPage.getKeyInput();
    const loginButton = loginPage.getLoginButton();

    await input.sendKeys('abcd-egfh-ijkl-mnop');

    await loginButton.click();

    await browser.wait(protractor.ExpectedConditions.urlContains('home'));
  });
});
