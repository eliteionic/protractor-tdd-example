import { LessonSelectPageObject } from './lesson-select.po';
import { LessonPageObject } from './lesson.po';

describe('Lesson Select', () => {
  let lessonSelectPage: LessonSelectPageObject;
  let lessonPage: LessonPageObject;

  beforeEach(async () => {
    lessonSelectPage = new LessonSelectPageObject();
    lessonPage = new LessonPageObject();
    await lessonSelectPage.navigateTo();
  });

  it('the list of lessons should contain the titles of the lessons', async () => {
    expect(
      await lessonSelectPage.getLessonListItems().first().getText()
    ).toContain('lesson 1');
  });

  it('after selecting a specific lesson, the user should be able to see content for that lesson', async () => {
    await lessonSelectPage.getLessonListItems().first().click();

    expect(await lessonPage.getLessonContent().getText()).toContain(
      'this is the lesson content'
    );
  });
});
