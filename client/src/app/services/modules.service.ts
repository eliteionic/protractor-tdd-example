import { Injectable } from '@angular/core';
import { Lesson } from '../interfaces/lesson';
import { Module } from '../interfaces/module';

@Injectable({
  providedIn: 'root',
})
export class ModulesService {
  constructor() {}

  getModules(): Module[] {
    const modules = [
      {
        id: 1,
        title: 'Module One',
        description: 'Test',
        lessons: [
          { id: 1, title: 'lesson 1', content: 'this is the lesson content' },
          { id: 2, title: 'lesson 2', content: 'this is the lesson content' },
        ],
      },
      {
        id: 2,
        title: 'Module Two',
        description: 'Test',
        lessons: [
          { id: 1, title: 'lesson 1', content: 'this is the lesson content' },
          { id: 2, title: 'lesson 2', content: 'this is the lesson content' },
        ],
      },
      {
        id: 3,
        title: 'Module Three',
        description: 'Test',
        lessons: [
          { id: 1, title: 'lesson 1', content: 'this is the lesson content' },
          { id: 2, title: 'lesson 2', content: 'this is the lesson content' },
        ],
      },
      {
        id: 4,
        title: 'Module Four',
        description: 'Test',
        lessons: [
          { id: 1, title: 'lesson 1', content: 'this is the lesson content' },
          { id: 2, title: 'lesson 2', content: 'this is the lesson content' },
        ],
      },
      {
        id: 5,
        title: 'Module Five',
        description: 'Test',
        lessons: [
          { id: 1, title: 'lesson 1', content: 'this is the lesson content' },
          { id: 2, title: 'lesson 2', content: 'this is the lesson content' },
        ],
      },
    ];

    return modules;
  }

  getModuleById(id: number): Module {
    const moduleToReturn = this.getModules().find((courseModule) => {
      return courseModule.id == id;
    });

    return moduleToReturn;
  }

  getLessonById(moduleId: number, lessonId: number): Lesson {
    const moduleWithLesson = this.getModules().find((courseModule) => {
      return courseModule.id == moduleId;
    });

    const lessonToReturn = moduleWithLesson.lessons.find((lesson) => {
      return lesson.id == lessonId;
    });

    return lessonToReturn;
  }
}
