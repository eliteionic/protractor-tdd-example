import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public _storage: Storage;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private navCtrl: NavController
  ) {
    this.init();
  }

  async init(): Promise<void> {
    this._storage = await this.storage.create();
  }

  checkKey(key: string): Observable<any> {
    this._storage.set('eliteLicenseKey', key);

    const body = {
      key: key,
    };

    return this.http.post('http://localhost:8080/api/check', body);
  }

  async reauthenticate(): Promise<void> {
    await this.init();
    const key = await this._storage.get('eliteLicenseKey');

    if (!key) {
      throw new Error('No key found');
    }

    this.checkKey(key).subscribe((res) => {
      if (!res) {
        throw new Error('Invalid key');
      }
    });
  }

  async logout() {
    await this._storage.set('eliteLicenseKey', null);
    this.navCtrl.navigateRoot('/login');
  }
}
