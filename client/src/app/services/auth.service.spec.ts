import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { NavController } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage-angular';

import { AuthService } from './auth.service';
import { NavMock } from '../../../mocks/mocks-ionic';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],

      providers: [{ provide: NavController, useClass: NavMock }, AuthService],

      imports: [HttpClientTestingModule, IonicStorageModule.forRoot()],
    }).compileComponents();
  });

  it('checkKey should make a call to the server to check the validity of a key', inject(
    [AuthService, HttpTestingController],
    async (authService, httpMock) => {
      const key = 'ewu0fef0ewuf08j3892jf98';
      const mockResponse = '{"isValid": true}';

      await authService.init();

      authService.checkKey(key).subscribe((result: any) => {
        expect(result).toEqual(mockResponse);
      });

      // Expect a request to the URL
      const mockReq = httpMock.expectOne('http://localhost:8080/api/check');

      // Execute the request using the mockResponse data
      mockReq.flush(mockResponse);
    }
  ));

  it('reauthenticate should automatically check key if in storage', inject(
    [AuthService, HttpTestingController],
    async (authService, httpMock) => {
      const mockResponse = '{"isValid": true}';

      await authService.init();

      spyOn(authService._storage, 'get').and.returnValue(
        Promise.resolve('abcde-fghi-jklm')
      );

      await authService.reauthenticate();

      // Expect a request to the URL
      const mockReq = httpMock.expectOne('http://localhost:8080/api/check');

      // Execute the request using the mockResponse data
      mockReq.flush(mockResponse);
    }
  ));

  it('checkKey should save the key being checked to storage', inject(
    [AuthService, HttpTestingController],
    async (authService, httpMock) => {
      const key = 'ewu0fef0ewuf08j3892jf98';
      const mockResponse = '{"isValid": true}';

      await authService.init();

      spyOn(authService._storage, 'set');

      authService.checkKey(key).subscribe((result: any) => {
        expect(result).toEqual(mockResponse);
      });

      // Expect a request to the URL
      const mockReq = httpMock.expectOne('http://localhost:8080/api/check');

      // Execute the request using the mockResponse data
      mockReq.flush(mockResponse);

      expect(authService._storage.set).toHaveBeenCalledWith(
        'eliteLicenseKey',
        'ewu0fef0ewuf08j3892jf98'
      );
    }
  ));

  it('the logout function should set the root page to the Login Page', inject(
    [AuthService, NavController],
    async (authService, navCtrl) => {
      await authService.init();

      spyOn(authService._storage, 'set').and.returnValue(Promise.resolve(true));
      spyOn(navCtrl, 'navigateRoot');

      await authService.logout();

      expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/login');
    }
  ));

  it('the logout function should clear the key in storage', inject(
    [AuthService],
    async (authService) => {
      await authService.init();
      spyOn(authService._storage, 'set').and.returnValue(Promise.resolve(true));

      authService.logout();

      expect(authService.storage.set).toHaveBeenCalledWith(
        'eliteLicenseKey',
        null
      );
    }
  ));
});
