import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../services/modules.service';
import { ActivatedRoute } from '@angular/router';
import { Lesson } from '../interfaces/lesson';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.page.html',
  styleUrls: ['./lesson.page.scss'],
})
export class LessonPage implements OnInit {
  public lesson: Lesson = { id: 0, title: '', content: '' };

  constructor(
    private modulesService: ModulesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const moduleId = this.route.snapshot.paramMap.get('moduleId');
    const lessonId = this.route.snapshot.paramMap.get('lessonId');
    this.lesson = this.modulesService.getLessonById(
      parseInt(moduleId),
      parseInt(lessonId)
    );
  }
}
