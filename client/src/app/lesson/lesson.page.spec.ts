import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { ModulesService } from '../services/modules.service';
import { ModulesMock } from '../../../mocks/mocks-app';

import { LessonPage } from './lesson.page';

describe('LessonPage', () => {
  let component: LessonPage;
  let fixture: ComponentFixture<LessonPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [LessonPage],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              snapshot: {
                paramMap: {
                  get: (path) => {
                    return '1';
                  },
                },
              },
            },
          },
          { provide: ModulesService, useClass: ModulesMock },
        ],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(LessonPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a lesson class member', () => {
    expect(component.lesson).toBeDefined();
  });

  it('should set up the passed in lesson as the lesson class member', () => {
    const modulesService: any = fixture.debugElement.injector.get(
      ModulesService
    );

    modulesService.getLessonById = jasmine
      .createSpy('getLessonById')
      .and.returnValue({
        id: 1,
        title: 'lesson 1',
        content: 'this is the test content',
      });

    component.ngOnInit();

    expect(component.lesson.title).toBe('lesson 1');
  });
});
