import {
  ComponentFixture,
  TestBed,
  waitForAsync,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';
import { NavMock } from '../../../mocks/mocks-ionic';
import { AuthService } from '../services/auth.service';
import { ModulesService } from '../services/modules.service';
import { ModulesMock } from '../../../mocks/mocks-app';
import { AuthMock } from '../../../mocks/mocks-app';

import { HomePage } from './home.page';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [HomePage],
        providers: [
          { provide: AuthService, useClass: AuthMock },
          { provide: ModulesService, useClass: ModulesMock },
          { provide: NavController, useClass: NavMock },
        ],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(HomePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a class member called modules that is an array', () => {
    expect(component.modules instanceof Array).toBe(true);
  });

  it('the modules class member should contain 5 modules after ngOnInit has been triggered', () => {
    const moduleProvider = fixture.debugElement.injector.get(ModulesService);

    const dummyModule = {
      id: 0,
      title: '',
      description: '',
      lessons: [],
    };

    spyOn(moduleProvider, 'getModules').and.returnValue(
      new Array(5).fill(dummyModule)
    );

    component.ngOnInit();

    expect(component.modules.length).toBe(5);
  });

  it('the openModule() function should navigate to the LessonListPage', () => {
    const navCtrl = fixture.debugElement.injector.get(NavController);

    spyOn(navCtrl, 'navigateForward');

    const testModule = { title: 'pretend module', id: 1 };

    component.openModule(testModule.id);

    expect(navCtrl.navigateForward).toHaveBeenCalledWith(
      '/module/' + testModule.id
    );
  });

  it('the logout function should call the logout method of the auth provider', () => {
    const authProvider = fixture.debugElement.injector.get(AuthService);

    spyOn(authProvider, 'logout');

    component.logout();

    expect(authProvider.logout).toHaveBeenCalled();
  });
});
