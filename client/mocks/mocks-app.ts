export class ModulesMock {
  public getModules(): Object[] {
    return [{}];
  }

  public getModuleById(id: number): Object {
    return {};
  }

  public getLessonById(moduleId: number, lessonId: number): Object {
    return {};
  }
}

export class AuthMock {
  public checkKey(key: string): any {}

  public reauthenticate(): any {}

  public logout(): any {}
}
